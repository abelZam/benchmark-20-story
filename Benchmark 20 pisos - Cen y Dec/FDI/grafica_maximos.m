

%% grafica de maximos
pisos = 1:1:2;
%maximos = maximos*1000;

plot(maximos(1,:), pisos, '*-.')
hold on
plot(maximos(2,:), pisos, '+-.')
plot(maximos(3,:), pisos, 'o-.')
plot(maximos(4,:), pisos, '.-.')
ylabel('Story number','Interpreter','latex')
xlabel('ISD (mm)','Interpreter','latex')
legend('Designed $u_1$', '$u_1$ with frequency 7 rad/s', '$u_1$ with frequency 8 rad/s','$u_1$ with frequency 9 rad/s','Location','southwest','Interpreter','latex')
%legend('With false $u_1$, $u_2$', 'With false $u_1$, $u_3$', 'With false $u_2$, $u_3$','Location','southeast','Interpreter','latex')
%legend('With false $u_1$', 'With false $u_2$','Interpreter','latex')
%legend('Without $u_1$', 'Without $u_2$','Interpreter','latex')
title('Maximum ISD per Story for different $u_1$ signals','Interpreter','latex')
yticks(pisos)
