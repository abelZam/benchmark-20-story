function fobj = funobj(seleccion, A, B, C, D, Gain)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    matrix_mask = diag(1-seleccion);
    K = matrix_mask*Gain;
    Acl = A + B*K;
    Ccl = C + D*K;
    siscl = ss(Acl,B,Ccl,D);
    tranfer = tf(siscl);
    
    fobj = -hinfnorm( tranfer(1:20,find(seleccion))); 
