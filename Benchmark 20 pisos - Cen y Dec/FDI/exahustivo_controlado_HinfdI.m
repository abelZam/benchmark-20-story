clear global
clc

% numero de pisos y parametros f�sicos del edificio
n = 3;
nx = 2*n;
nu = n;
ny = 2*n;
h = 3; % altura de cada piso del edificio (metros)

M = eye(n)*6000; %kg
K = [3.4 -1.8  0;   ...
    -1.8  3.4 -1.6; ...
     0   -1.6  1.6 ]*10^6; % N/m
C = [12.4  -5.16 0;   ...
     -5.16 12.4 -4.59;...
      0    -4.59 7.20   ]*10^3; % N/(m/s)
Tu = -1*diag(ones(1,n));
for i = 2:n
    Tu(i-1,i) = 1;
end
     %[ -1  1  0 ;...
     %   0 -1  1 ;...
     %   0  0 -1 ];

% Modelo teniendo en cuenta posiciones y velocidades absolutas
%Minv = inv(M);
AI = [zeros(n) eye(n);-M\K -M\C];
BI = [zeros(n); M\Tu];
EI = [zeros(n,1); -1*ones(n,1)];

% Transformaci�n de las variables de estado para que sean inter story drift
% T= [      1  0 0  0  0 0   ;  
%           0  0 0  1  0 0   ;
%          -1  1 0  0  0 0   ; 
%           0  0 0 -1  1 0   ; 
%           0 -1 1  0  0 0   ;  
%           0  0 0  0 -1 1  ];

A = -1*Tu';
B = zeros(n);
T = B([1;1]*(1:size(B,1)),:);
T(1:2:end,:) = A;
T(2:end,n+1:2*n) = T(1:end-1,:);

A = T*AI/T;
B = T*BI;
E = T*EI;

% matrices de la ecuaci�n de salida
% C = [1 0 0 0 0 0;0 0 1 0 0 0; 0 0 0 0 1 0];
aux = zeros(n);
C = aux(:,[1;1]*(1:size(aux,2)));
C(:,1:2:end) = eye(n);
D = zeros(n,1);

% sistema en lazo abierto sin control u= 0
sisLA = ss(A, E, C ,D);

% limpia variables que no son necesarias mas adelante
clear M K Tu Minv AI BI EI i aux

%% Matrices Cz and Dz chosen in paper Yang Wang
Cz = zeros(ny,nx);
Cz(1,1) = 50; Cz(2,3)=50; Cz(3,5)=50;
Dz = zeros(ny,nu);
Dz(4,1)=3.162; Dz(5,2)=3.162; Dz(6,3)=3.162;
Dz = Dz*10^-5;
D1 = zeros(nx,1); %#ok<PREALL>

%%
sisLA_SC = ss(A, B, Cz, Dz);

%% An�lisis frecuencial a trav�s de SVD  
% restricci�n

tranfer = tf(sisLA_SC);
[mag, fpeak] = hinfnorm(tranfer(1:3,:));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:3,:);
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%%
[vec, maxi] = mejores_pisos_atacar(sisLA_SC, rand(3,6), 3,3);
20*log10(maxi)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:10;
u1 = amp(1)*10^4*sin(fpeak*tiempo2+fase(1)); 
u2 = amp(2)*10^4*sin(fpeak*tiempo2+fase(2)); 
u3 = amp(3)*10^4*sin(fpeak*tiempo2+fase(3));

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
out = lsim(sisLA_SC,u_atq(:,2:4), u_atq(:,1));
max(abs(out(:,1:3)/50/h*100))
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")


figure
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
axis([0 5 -inf inf])

clear u1 u2 u3 


%% Ganancia full state feedback H_inf dI con u = Gainx + u'
Gain = [-1.578 5.608 0 0 0 0; 0 0 -5.383 3.195 0 0; 0 0 0 0 0 0]*10^5;

%sistema con retroalimentaci�n de estado H_inf
Acl = A + B*Gain;
Ccl = Cz+ Dz*Gain;
sisH_inf1u = ss(Acl,B(:,1:3),Ccl,Dz(:,1:3));

%% An�lisis frecuencial a trav�s de SVD  % no muy efectivo por que la frecuencia es la misma para todas las entradas
% restricci�n

tranfer = tf(sisH_inf1u);
[mag, fpeak] = hinfnorm(tranfer(1:3,3));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:3,3);
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:10;
u1 = 0*10^4*sin(fpeak*tiempo2+fase); 
u2 = 0*10^4*sin(fpeak*tiempo2+fase); 
u3 = amp*10^4*sin(fpeak*tiempo2+fase);

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
figure
set(gcf,'Position',[100 100 1200 300])
out = lsim(sisH_inf1u,u_atq(:,2:4), u_atq(:,1));
maximos(3,:) = max(abs(out(:,1:3)/50/h*100));
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")


figure
set(gcf,'Position',[100 100 1200 300])
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
%axis([0 30 -inf inf])

clear u1 u2 u3 

%% Ganancia full state feedback H_inf dI con u = Gainx + u'
Gain = [-1.578 5.608 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 -4.109 3.013]*10^5;

%sistema con retroalimentaci�n de estado H_inf
Acl = A + B*Gain;
Ccl = Cz+ Dz*Gain;
sisH_inf1u = ss(Acl,B(:,1:3),Ccl,Dz(:,1:3));

%% An�lisis frecuencial a trav�s de SVD  % no muy efectivo por que la frecuencia es la misma para todas las entradas
% restricci�n

tranfer = tf(sisH_inf1u);
[mag, fpeak] = hinfnorm(tranfer(1:3,:));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:3,2);
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:10;
u1 = 0*10^4*sin(fpeak*tiempo2+fase); 
u2 = amp*10^4*sin(fpeak*tiempo2+fase); 
u3 = 0*10^4*sin(fpeak*tiempo2+fase);

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
out = lsim(sisH_inf1u,u_atq(:,2:4), u_atq(:,1));
maximos(2,:) = max(abs(out(:,1:3)/50/h*100))
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")


figure
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
%axis([0 30 -inf inf])

clear u1 u2 u3 

%% Ganancia full state feedback H_inf dI con u = Gainx + u'
Gain = [0 0 0 0 0 0; 0 0 -5.383 3.195 0 0; 0 0 0 0 -4.109 3.013]*10^5;

%sistema con retroalimentaci�n de estado H_inf
Acl = A + B*Gain;
Ccl = Cz+ Dz*Gain;
sisH_inf1u = ss(Acl,B(:,1:3),Ccl,Dz(:,1:3));


%% An�lisis frecuencial a trav�s de SVD  % no muy efectivo por que la frecuencia es la misma para todas las entradas
% restricci�n

tranfer = tf(sisH_inf1u);
[mag, fpeak] = hinfnorm(tranfer(1:3,1));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:3,1);
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:10;
u1 = amp*10^4*sin(fpeak*tiempo2+fase); 
u2 = 0*10^4*sin(fpeak*tiempo2+fase); 
u3 = 0*10^4*sin(fpeak*tiempo2+fase);

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
out = lsim(sisH_inf1u,u_atq(:,2:4), u_atq(:,1));
maximos(1,:) = max(abs(out(:,1:3)/50/h*100));
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")

figure
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
%axis([0 30 -inf inf])

clear u1 u2 u3 

%%






Gain = [-1.578 5.608 0 0 0 0; 0 0 -5.383 3.195 0 0; 0 0 0 0 -4.109 3.013]*10^5;

[vec, maxi, normas] = mejores_pisos_atacar(sisLA_SC, Gain, 3,1);
20*log10(maxi)

xlim([0.1 500])
ejey = axis;
ylim([ejey(3) ejey(4)+10])
title('Frequency Response','Interpreter', 'latex')
ylabel('Largest Singular Value (dB)', 'Interpreter', 'latex')
xlabel('Frequency (Hz)', 'Interpreter', 'latex')
legend(['False $u_1$, $H_\infty$ norm = ', num2str(20*log10(normas(1))), ' dB'], ['False $u_2$, $H_\infty$ norm =    \hspace{0.15cm}', num2str(20*log10(normas(2))), ' dB'], ['False $u_3$, $H_\infty$ norm = ', num2str(20*log10(normas(3))), ' dB'], 'Location','SouthWest', 'Interpreter', 'latex')








%% Ganancia full state feedback H_inf dI con u = Gainx + u'
Gain = [-1.578 5.608 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0]*10^5;

%sistema con retroalimentaci�n de estado H_inf
Acl = A + B*Gain;
Ccl = Cz+ Dz*Gain;
sisH_inf1u = ss(Acl,B(:,1:3),Ccl,Dz(:,1:3));

%% An�lisis frecuencial a trav�s de SVD  % no muy efectivo por que la frecuencia es la misma para todas las entradas
% restricci�n

tranfer = tf(sisH_inf1u);
[mag, fpeak] = hinfnorm(tranfer(1:3,[2,3]));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:3,2:3);
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:10;
u1 = 0*10^4*sin(fpeak*tiempo2+fase(1)); 
u2 = amp(1)*10^4*sin(fpeak*tiempo2+fase(1)); 
u3 = amp(2)*10^4*sin(fpeak*tiempo2+fase(2));

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
out = lsim(sisH_inf1u,u_atq(:,2:4), u_atq(:,1));
maximos(3,:)= max(abs(out(:,1:3)/50/h*100))
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")

figure
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
%axis([0 30 -inf inf])

clear u1 u2 u3

%% Ganancia full state feedback H_inf dI con u = Gainx + u'
Gain = [0 0 0 0 0 0; 0 0 -5.383 3.195 0 0; 0 0 0 0 0 0]*10^5;

%sistema con retroalimentaci�n de estado H_inf
Acl = A + B*Gain;
Ccl = Cz+ Dz*Gain;
sisH_inf1u = ss(Acl,B(:,1:3),Ccl,Dz(:,1:3));

%% An�lisis frecuencial a trav�s de SVD  % no muy efectivo por que la frecuencia es la misma para todas las entradas
% restricci�n

tranfer = tf(sisH_inf1u);
[mag, fpeak] = hinfnorm(tranfer(1:3,[1,3]));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:3,1:2:3);
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:10;
u1 = amp(1)*10^4*sin(fpeak*tiempo2+fase(1)); 
u2 = 0*10^4*sin(fpeak*tiempo2+fase(2)); 
u3 = amp(2)*10^4*sin(fpeak*tiempo2+fase(2));

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
out = lsim(sisH_inf1u,u_atq(:,2:4), u_atq(:,1));
maximos(2,:)=max(abs(out(:,1:3)/50/h*100))
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")

figure
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
%axis([0 30 -inf inf])

clear u1 u2 u3


%% Ganancia full state feedback H_inf dI con u = Gainx + u'
Gain = [0 0 0 0 0 0; 0 0 0 0 0 0;  0 0 0 0 -4.109 3.013]*10^5;

%sistema con retroalimentaci�n de estado H_inf
Acl = A + B*Gain;
Ccl = Cz+ Dz*Gain;
sisH_inf1u = ss(Acl,B(:,1:3),Ccl,Dz(:,1:3));

%% An�lisis frecuencial a trav�s de SVD  % no muy efectivo por que la frecuencia es la misma para todas las entradas
% restricci�n

tranfer = tf(sisH_inf1u);
[mag, fpeak] = hinfnorm(tranfer(1:3,[1,2]));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:3,1:2);
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:15;
u1 = amp(1)*10^4*sin(fpeak*tiempo2+fase(1)); 
u2 = amp(2)*10^4*sin(fpeak*tiempo2+fase(2)); 
u3 = 0*10^4*sin(fpeak*tiempo2+fase(1));

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
out = lsim(sisH_inf1u,u_atq(:,2:4), u_atq(:,1));
maximos(1,:) = max(abs(out(:,1:3)/50/h*100))
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")

figure
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
%axis([0 30 -inf inf])

clear u1 u2 u3

%%
Gain = [-1.578 5.608 0 0 0 0; 0 0 -5.383 3.195 0 0; 0 0 0 0 -4.109 3.013]*10^5;

[vec, maxi, normas] = mejores_pisos_atacar(sisLA_SC, Gain, 3,2);
20*log10(maxi)

xlim([0.1 500])
ejey = axis;
ylim([ejey(3) ejey(4)+10])
title('Frequency Response','Interpreter', 'latex')
ylabel('Largest Singular Value (dB)', 'Interpreter', 'latex')
xlabel('Frequency (Hz)', 'Interpreter', 'latex')
legend(['False $u_1$, $u_2$, $H_\infty$ norm = ', num2str(20*log10(normas(1))), ' dB'], ['False $u_1$, $u_3$, $H_\infty$ norm = ', num2str(20*log10(normas(2))), ' dB'], ['False $u_2$, $u_3$, $H_\infty$ norm = ', num2str(20*log10(normas(3))), ' dB'],'Location', 'southwest', 'Interpreter', 'latex')

%% No funciona

cvx_begin sdp
    variable seleccion(1,nu) binary
    variable frecuencia
    matrix_mask = diag(seleccion);
    
    K = Gain;
    Acl = A + B*K;
    Ccl = Cz+ Dz*K;
    siscl = ss(Acl,B,Ccl,Dz);
    tranfer = tf(siscl);
    T = evalfr(tranfer, frecuencia*i);
    H = T*matrix_mask ;
    
    minimize((norm( H , Inf)))
    subject to:
    Acl >= 0;
    trace(matrix_mask) >=1;
cvx_end

seleccion


%% Prueba fmincon

li = zeros(1,n);
ls = ones(1,n);
Ae = [1 1 1];
Be = [2];
iteraciones=15;
options = optimoptions('fmincon','Algorithm','interior-point'); 
costofinal = 0;
selfinal = [];
tic
for i=1:iteraciones
seleccion = rand(1,n);
[Selopt, costo] = fmincon(@funobj, seleccion, [],[], Ae,Be,li,ls,[], options);
if costo < costofinal
    costofinal = costo;
    selfinal = Selopt;
end
end
toc
selfinal, costofinal

%% Prueba genetic algoritm

li = zeros(1,n);
ls = ones(1,n);
Ae = [ones(1,n); -ones(1,n)];
Be = 5;
opts = optimoptions(@ga,'UseParallel', true, 'UseVectorized', false, ...
                    'MaxGenerations', 15, 'PlotFcn', @gaplotbestf);

IntCon = 1:n;
tic
costos =zeros(n,1);
selecciones =zeros(n,n);
for k=1:n
Be = [k;-k];
[Selopt, costo] = ga(@(x)funobj(x,A,B,Cz,Dz, Gain), n, Ae,Be, [],[], li, ls, [], IntCon, opts );
toc
costos(k) = costo;
selecciones(k,:) = Selopt;
end

%%

set(gcf,'position',[403   246   560   320])
plot(20*log10(-costos))
hold on
scatter(1:20,20*log10(-costos))
xlabel("$k$",'FontSize',13,'Interpreter','latex')
ylabel("H2 norm (dB)",'FontSize',13,'Interpreter','latex')

%% sistema con retroalimentaci�n de estado H_inf
combinaciones = find(Selopt);
Gaini = Gain;
Gaini(combinaciones,:)=0;

Acl = A + B*Gaini;
Ccl = Cz+ Dz*Gaini;
sisH_inf1u = ss(Acl,B(:,1:20),Ccl,Dz(:,1:20));

%% An�lisis frecuencial a trav�s de SVD  % no muy efectivo por que la frecuencia es la misma para todas las entradas
% restricci�n

tranfer = tf(sisH_inf1u);
[mag, fpeak] = hinfnorm(tranfer(1:20,find(Selopt)));
matrixtf = evalfr(tranfer, fpeak*1i);
matrixtf = matrixtf(1:20,find(Selopt));
[U,S,V] = svd(matrixtf);

amp = abs(V(:,1));
fase = angle(V(:,1));

20*log10(mag)

%% Manipulaci�n de de la se�al de control u 

tiempo2 = 0:0.01:10;
u1 = amp(1)*10^4*sin(fpeak*tiempo2+fase(1)); 
u2 = amp(2)*10^4*sin(fpeak*tiempo2+fase(2)); 
u3 = amp(3)*10^4*sin(fpeak*tiempo2+fase(3));
u2 = amp(2)*10^4*sin(fpeak*tiempo2+fase(2)); 
u3 = amp(3)*10^4*sin(fpeak*tiempo2+fase(3));

%entradas senos 
u_atq = [tiempo2', u1', u2', u3'];

% simulacion sistema continuo con full state feedback Gain LQR dI
figure
set(gcf,'Position',[100 100 1200 300])
out = lsim(sisH_inf1u,u_atq(:,2:4), u_atq(:,1));
maximos(3,:) = max(abs(out(:,1:3)/50/h*100));
plot(u_atq(:,1),out(:,1:3)/50/h*100)
xlabel("Tiempo (s)")
ylabel("ISD ratio (%)")
legend("floor 1", "floor 2", "floor 3")


figure
set(gcf,'Position',[100 100 1200 300])
%max(abs(out(1:1500,4:6)/(3.162*10^-5)))
plot(tiempo2', out(:,4:6)/(3.162*10^-5))
xlabel("Tiempo (s)")
ylabel("Se�al de control, fuerza del actuador (N)")
legend("floor 1","floor 2","floor 3")
%axis([0 30 -inf inf])

clear u1 u2 u3 

